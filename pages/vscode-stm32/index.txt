Небольшая заметка о том, как я решил отказаться от keil и перешел на visual studio code.

Давно я собирался это сделать, но никак не мог решиться. Больше всего в vscode привлекал удобный редактор с его плюшками, до которого keil'у, да и многим другим IDE расти и расти.

Основной проблемой было то, как настроить сборку, как выполнять загрузку и отладку. "У страха глаза велики" - это правда, на деле всё оказалось куда проще. Даже этот текст, который вы читаете, написан в vscode.

Если стало интересно - читайте дальше.

<!-- pagebreak -->

#### Установка VS Code и расширений

Установка самого редактора тривиальна, скачать можно по ссылке: <https://code.visualstudio.com>

Для разработки приложений на языке Си для микроконтроллеров ARM будем использовать несколько плагинов:

* [Cortex-Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug)
* [C/C++ for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
* [C/C++ Intellisense](https://marketplace.visualstudio.com/items?itemName=austin.code-gnu-global)
* [ARM assembly highlighting for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=dan-c-underwood.arm)
* [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
* [Doxygen Documentation Generator](https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen)
* [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)

Основные - это верхние три, остальные это лишь небольшие улучшения и дополнения, хотя в принципе достаточно и первых двух.

#### Настройка среды

Вся настройка производится в файлах `json`.

Файл `.vscode\launch.json` соделжит конфигурации запуска и отладки.

Пример моей конфигурации:

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "cortex-debug",
            "name": "APP",
            "cwd": "${workspaceRoot}",
            "device": "Cortex-M0",
            "executable": "./build/smart_relay.elf",
            "servertype": "jlink",
            "interface": "swd",
            "request": "launch",
            "rtos": "FreeRTOS",
            "svdFile": "./JLink/STM32F042x.svd",
        }
    ]
}
```

Это конфигурация для плагина Cortex-Debug, в которой указаны параметры типа ядра, путь к elf файлу, ипя конфигурации для отладки, интерфейс, тип операционной системы, тип отладчика, путь к файлу описания регистров микроконтроллера (SVD файл), его можно найти в папке с файлами поддержки семейства микроконтроллеров для STM32CubeMx, например для `STM32F0` здесь

<https://github.com/STMicroelectronics/STM32CubeF0.git>

Следующий файл - `.vscode\settings.json`, - настройки рабочего пространства, у меня выглядят примерно так.

```json
{
    "cortex-debug.JLinkGDBServerPath": "JLinkGDBServerCL.exe",
    "cortex-debug.armToolchainPath": "c:\\gcc-arm\\bin",
    "project_mcu": "STM32F042K6"
}
```

Указаны лишь три настройки, могут быть и другие, но эти важны для нашего проекта.

Теперь приступим к настройкам нашего проекта, а именно укажем пути к заголовочным файлам, дефайны и настройки стандарта языка, это всё нужно не для сборки проекта, а для работы системы IntelliSense. Главное быть внимательным, так как эти настройки должны быть максимально синхронны с настройками при компилировании файлов, вплоть до указания предопределенных макросов типа `__GNU__` и подобных. Иначе можно получить весьма неожиданный результат, на экране будет одно, а по факту проект будет собираться уже по другом из-за вмешательства препроцессора и раскрытия макросов.

```json
{
    "configurations": [
        {
            "name": "Smart Relay App",
            "compilerPath": "${PATH}/arm-none-eabi-gcc.exe",
            "compilerArgs": ["--target=arm-arm-none-eabi", "-mcpu=Cortex-M0"],
            "includePath": [
                "c:/gcc-arm/lib/gcc/arm-none-eabi/9.2.1/include",
                "c:/gcc-arm/lib/gcc/arm-none-eabi/9.2.1/include-fixed",
                "${workspaceRoot}/libs/FreeRTOS-Kernel/include",
                "${workspaceRoot}/libs/FreeRTOS-Kernel/portable/GCC/ARM_CM0",
                "${workspaceRoot}/libs/STM32CubeF0_HAL/Drivers/CMSIS/Core/Include",
                "${workspaceRoot}/libs/STM32CubeF0_HAL/Drivers/CMSIS/Device/ST/STM32F0xx/Include",
                "${workspaceRoot}/libs/STM32CubeF0_HAL/Drivers/CMSIS/Include",
                "${workspaceRoot}/libs/STM32CubeF0_HAL/Drivers/STM32F0xx_HAL_Driver/Inc",
                "${workspaceRoot}",
                "${workspaceRoot}/src",
                "${workspaceRoot}/src/drivers",
                "${workspaceRoot}/src/app"
            ],
            "defines": [
                "STM32F042x6", "USE_HAL_DRIVER", "_RTE_", "USE_FULL_ASSERT"
            ],
            "cStandard": "c11",
            "cppStandard": "c++17",
            "intelliSenseMode": "${default}"
        }
    ],
    "version": 4
}
```

Из названия настроек всё понятно: имя конфигурации, путь к компилятору, его аргументы, пути к заголовочным файлам, опеределения макросов и указание стандарта языка. Более подробно об этих параметрах можно почитать в официальной документации.

В принципе все основные настройки готовы, осталось определелить несколько задач для сборки и загрузки прошивки в микроконтроллер. Для этого их необходимо описать в файле `.vscode\tasks.json`.

```json
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "Build",
            "type": "shell",
            "command": "make -s -j 8 app",
            "options": {
                "cwd": "${workspaceRoot}",
                "shell": {
                    "executable": "cmd.exe",
                    "args": [ "/C" ]
                }
            },
            "group": {
                "kind": "build",
                "isDefault": true
            },
            "problemMatcher": [
                "$go"
            ]
        },
        {
            "label": "Download",
            "type": "shell",
            "command": "JLink.exe -Device STM32F042K6 -If SWD -Speed 4000 JLink\\FlashMCU.jlink",
            "options": {
                "cwd": "${workspaceRoot}",
                "shell": {
                    "executable": "cmd.exe",
                    "args": [ "/C" ]
                }
            },
            "group": {
                "kind": "test",
                "isDefault": true
            },
            "problemMatcher": []
        }
    ]
}
```

Первая задача `Build` - задача сборки, о чем намекает параметр `"kind": "build"`, именно её редактор будет предлагать к выбору при выборе меню `Терминал -> Запустить задачу сборки...`, либо при нажатии кнопок `CTRL+SHIFT+B`.

Команда которая будет выполняться написана в параметре `command`.

В первой задаче это запуск утилиты `make` с параметрами.

Во второй это запуск утилиты `jlink` для записи файла прошивки в микроконтроллер. Видно, что здесь указаны тип устройства, интерфейс и скорость соединения и скрипт прошивки. Скрипт выглядит следующим образом:

```
r
loadfile .\build\smart_relay.hex
r
g
q
```

Другие команды, для чтения, очистки и сброса можно посмотреть в документации к `jlink`.

Ну вот и всё, Вам же осталось только написать свой код, написать `Makefile` и пользоваться средой.

#### Немного об ощущениях

Плюсы:

* Превосходный редактор с отличным функционалом;
* Можно переходить к определнию функции по клику с нажатым `CTRL`;
* куча расширений функционала редактора.

Минусы:

* Немного непривычная отладка: значения регистров и переменных в десятичном формате;
* Отладка без дизассемблера;
* Возможность отстрелить ногу с предопределенными компилятором макросами (то, о чем я писал выше);
* Много ручной работы по настройке проекта и системы сборки;
* GCC вместо ARM Compiler 6.

На первый взгляд кажется что минусов больше, но на самом деле удобство работы в редакторе кода компенсирует многое. А расширение функции отладки скорее всего дело будущих версий этого плагина.

#### PS

Как всегда хорошего кодинга и меньше багов.
