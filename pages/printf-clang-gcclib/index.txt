Не так давно я писал о том как перешел на [Clang/LLVM](http://cdeblog.ru/clang-llvm-stm32) и [Visual Studio Code](http://cdeblog.ru/vscode-stm32). Есть ещё один минус ко всему прочему, так как теперь используются либы от gcc не работает printf, запуск которого я описывал в статье "[printf в микроконтроллере STM32 и других](http://cdeblog.ru/printf-in-mcu)".

<!-- pagebreak -->

Для решения проблемы необходимо изменить определение нашиф функций `fputc` и `fgetc` следующим образом:

```c
#ifdef __GNUC__
int __io_putchar(int ch)
#else
int fputc(int ch, FILE *f)
#endif
{
    ...
}

#ifdef __GNUC__
int __io_getchar(void)
#else
int fgetc(int ch, FILE *f)
#endif
{
    ...
}
```

Что это даёт? Универсальность, если определен макрос `__GNU__` функция будет определена одинм образом, во всех остальных случаях другим, то есть данный код можно скормить и `GCC`, и `Clang`, и `Arm Compiler`.

Выглядит всё просто, но это ещё не все. Запустить таким образом не получилось, библиотека `libnosys` из `arm-none-eabi-gcc` не дала нужного результата (~~но может у меня руки кривые~~).

Пришлось реализовать несколько функций, что бы `printf` всё-таки заработал:

```c
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>

extern int errno;
extern int __io_putchar(int ch) __attribute__((weak));
extern int __io_getchar(void) __attribute__((weak));

register char * stack_ptr __asm("sp");

char *__env[1] = { 0 };
char **environ = __env;

void initialise_monitor_handles()
{
}

int _getpid(void)
{
    return 1;
}

int _kill(int pid, int sig)
{
    errno = EINVAL;
    return -1;
}

void _exit (int status)
{
    _kill(status, -1);
    while (1) {}
}

__attribute__((weak)) int _read(int file, char *ptr, int len)
{
    int DataIdx;

    for (DataIdx = 0; DataIdx < len; DataIdx++)
    {
        *ptr++ = __io_getchar();
    }
    
    return len;
}

__attribute__((weak)) int _write(int file, char *ptr, int len)
{
    int DataIdx;

    for (DataIdx = 0; DataIdx < len; DataIdx++)
    {
        __io_putchar(*ptr++);
    }
    return len;
}

caddr_t _sbrk(int incr)
{
    extern char end __asm("end");
    static char *heap_end;
    char *prev_heap_end;

    if (heap_end == 0)
        heap_end = &end;

    prev_heap_end = heap_end;

    if (heap_end + incr > stack_ptr)
    {
        errno = ENOMEM;
        return (caddr_t) -1;
    }

    heap_end += incr;

    return (caddr_t) prev_heap_end;
}

int _close(int file)
{
    return -1;
}


int _fstat(int file, struct stat *st)
{
    st->st_mode = S_IFCHR;
    return 0;
}

int _isatty(int file)
{
    return 1;
}

int _lseek(int file, int ptr, int dir)
{
    return 0;
}

int _open(char *path, int flags, ...)
{
    return -1;
}

int _wait(int *status)
{
    errno = ECHILD;
    return -1;
}

int _unlink(char *name)
{
    errno = ENOENT;
    return -1;
}

int _times(struct tms *buf)
{
    return -1;
}

int _stat(char *file, struct stat *st)
{
    st->st_mode = S_IFCHR;
    return 0;
}

int _link(char *old, char *new)
{
    errno = EMLINK;
    return -1;
}

int _fork(void)
{
    errno = EAGAIN;
    return -1;
}

int _execve(char *name, char **argv, char **env)
{
    errno = ENOMEM;
    return -1;
}
```

Данный код так же можно найти в папке с примерами от `ST` например здесь:

`\Examples\UART\UART_Printf\SW4STM32\syscalls.c`

Но нужно немного его изменить. Везде где встречается `asm("...")` нужно написать `__asm("...")`.

Вот и всё.
